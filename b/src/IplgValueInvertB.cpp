#include "stdafx.h"
#include "IplgValueInvertB.h"
#include "interplugin_test_b/interplugin_test_b.h"
#include "interplugin_test_a/IplgDemoCall.h"

using namespace megamol;

interplugin_test_b::IplgValueInvertB::IplgValueInvertB()
	: Module(),
		valueOutSlot("valueOut", ""),
		valueInSlot("valueIn", "") {

	valueOutSlot.SetCallback(interplugin_test_a::IplgDemoCall::ClassName(), interplugin_test_a::IplgDemoCall::FunctionName(0), &IplgValueInvertB::getValue);
	MakeSlotAvailable(&valueOutSlot);

	valueInSlot.SetCompatibleCall<interplugin_test_a::IplgDemoCallDescription>();
	MakeSlotAvailable(&valueInSlot);

}

interplugin_test_b::IplgValueInvertB::~IplgValueInvertB() {
	Release();
}

bool interplugin_test_b::IplgValueInvertB::create(void) {
	return true;
}

void interplugin_test_b::IplgValueInvertB::release(void) {
}

bool interplugin_test_b::IplgValueInvertB::getValue(core::Call& call) {
	interplugin_test_a::IplgDemoCall *o = dynamic_cast<interplugin_test_a::IplgDemoCall *>(&call);
	if (o == nullptr) return false;

	interplugin_test_a::IplgDemoCall *i = valueInSlot.CallAs<interplugin_test_a::IplgDemoCall>();
	if (i == nullptr) return false;
	if (!(*i)()) return false;

	o->SetValue(1000 - i->GetValue());

	return true;
}
