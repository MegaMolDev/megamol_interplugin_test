#pragma once
#include "mmcore/Call.h"
#include "mmcore/CalleeSlot.h"
#include "mmcore/CallerSlot.h"
#include "mmcore/Module.h"

namespace megamol {
namespace interplugin_test_b {

	class IplgValueInvertB : public core::Module {
	public:

		static const char *ClassName(void) { return "IplgValueInvertB"; }
		static const char *Description(void) { return "Module inverting a value"; }
		static bool IsAvailable(void) { return true; }

		IplgValueInvertB();
		virtual ~IplgValueInvertB();

	private:

		virtual bool create(void);
		virtual void release(void);

		bool getValue(core::Call& call);

		core::CalleeSlot valueOutSlot;
		core::CallerSlot valueInSlot;

	};

}
}
