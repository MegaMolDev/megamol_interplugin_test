#include "stdafx.h"
#include "IplgProviderAModule.h"
#include "mmcore/param/IntParam.h"
#include "interplugin_test_a/IplgDemoCall.h"

using namespace megamol;

interplugin_test_a::IplgProviderAModule::IplgProviderAModule()
	: core::Module(),
		getValueSlot("GetValue", ""),
		valueSlot("Value", "") {

	getValueSlot.SetCallback(IplgDemoCall::ClassName(), IplgDemoCall::FunctionName(0), &IplgProviderAModule::getValue);
	MakeSlotAvailable(&getValueSlot);

	valueSlot.SetParameter(new core::param::IntParam(0, 0, 1000));
	MakeSlotAvailable(&valueSlot);
}

interplugin_test_a::IplgProviderAModule::~IplgProviderAModule() {
	Release();
}

bool interplugin_test_a::IplgProviderAModule::create(void) {
	return true;
}

void interplugin_test_a::IplgProviderAModule::release(void) {
}

bool interplugin_test_a::IplgProviderAModule::getValue(core::Call& call) {
	IplgDemoCall *c = dynamic_cast<IplgDemoCall*>(&call);
	if (c == nullptr) return false;
	c->SetValue(valueSlot.Param<core::param::IntParam>()->Value());
	return true;
}
