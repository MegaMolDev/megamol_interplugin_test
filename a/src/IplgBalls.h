#pragma once
#include "mmcore/Call.h"
#include "mmcore/CalleeSlot.h"
#include "mmcore/CallerSlot.h"
#include "mmcore/Module.h"
#include <array>

namespace megamol {
namespace interplugin_test_a {

	class IplgBalls : public core::Module {
	public:

		static const char *ClassName(void) { return "IplgBalls"; }
		static const char *Description(void) { return "Module providing data for 0 to 1000 balls"; }
		static bool IsAvailable(void) { return true; }

		IplgBalls();
		virtual ~IplgBalls();

	private:

		virtual bool create(void);
		virtual void release(void);

		bool getData(core::Call& call);
		bool getExtent(core::Call& call);

		core::CallerSlot fetchValueSlot;
		core::CalleeSlot putDataSlot;

		std::array<float, 3000> posdat;
	};

}
}
