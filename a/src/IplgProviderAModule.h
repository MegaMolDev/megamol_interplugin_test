#pragma once
#include "mmcore/Call.h"
#include "mmcore/CalleeSlot.h"
#include "mmcore/Module.h"
#include "mmcore/param/ParamSlot.h"

namespace megamol {
namespace interplugin_test_a {

	class IplgProviderAModule : public core::Module {
	public:

		static const char *ClassName(void) { return "IplgProviderA"; }
		static const char *Description(void) { return "Module providing a value"; }
		static bool IsAvailable(void) { return true; }

		IplgProviderAModule();
		virtual ~IplgProviderAModule();

	private:

		virtual bool create(void);
		virtual void release(void);

		bool getValue(core::Call& call);

		core::CalleeSlot getValueSlot;
		core::param::ParamSlot valueSlot;

	};

}
}
