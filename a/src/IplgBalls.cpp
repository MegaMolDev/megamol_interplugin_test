#include "stdafx.h"
#include "IplgBalls.h"
#include "interplugin_test_a/IplgDemoCall.h"
#include "mmcore/moldyn/MultiParticleDataCall.h"

using namespace megamol;

interplugin_test_a::IplgBalls::IplgBalls()
	: Module(),
	fetchValueSlot("fetchvalue", ""),
	putDataSlot("putdata", ""),
	posdat() {

	fetchValueSlot.SetCompatibleCall<IplgDemoCallDescription>();
	MakeSlotAvailable(&fetchValueSlot);

	putDataSlot.SetCallback(core::moldyn::MultiParticleDataCall::ClassName(), "GetData", &IplgBalls::getData);
	putDataSlot.SetCallback(core::moldyn::MultiParticleDataCall::ClassName(), "GetExtent", &IplgBalls::getExtent);
	MakeSlotAvailable(&putDataSlot);
}


interplugin_test_a::IplgBalls::~IplgBalls() {
	Release();
}

bool interplugin_test_a::IplgBalls::create(void) {
	for (int z = 0; z < 10; ++z)
	for (int y = 0; y < 10; ++y)
	for (int x = 0; x < 10; ++x) {
		int i = (x * 10 + y) * 10 + z;
		posdat[i * 3 + 0] = 0.5f + static_cast<float>(x);
		posdat[i * 3 + 1] = 0.5f + static_cast<float>(y);
		posdat[i * 3 + 2] = 0.5f + static_cast<float>(z);
	}

	return true;
}

void interplugin_test_a::IplgBalls::release(void) {

}

bool interplugin_test_a::IplgBalls::getData(core::Call& call) {
	core::moldyn::MultiParticleDataCall* mpdc = dynamic_cast<core::moldyn::MultiParticleDataCall*>(&call);
	if (mpdc == nullptr) return false;

	int cnt = 0;

	IplgDemoCall *idc = fetchValueSlot.CallAs<IplgDemoCall>();
	if (idc != nullptr) {
		if ((*idc)()) cnt = idc->GetValue();
	}

	mpdc->SetFrameID(0);
	mpdc->SetParticleListCount(1);
	core::moldyn::SimpleSphericalParticles& p = mpdc->AccessParticles(0);

	p.SetCount(cnt);

	p.SetColourData(core::moldyn::SimpleSphericalParticles::COLDATA_NONE, nullptr);
	p.SetGlobalColour(64, 128, 255);
	p.SetGlobalRadius(0.5f);

	p.SetVertexData(core::moldyn::SimpleSphericalParticles::VERTDATA_FLOAT_XYZ, posdat.data());

	mpdc->SetDataHash(0);

	return true;
}

bool interplugin_test_a::IplgBalls::getExtent(core::Call& call) {
	core::moldyn::MultiParticleDataCall* mpdc = dynamic_cast<core::moldyn::MultiParticleDataCall*>(&call);
	if (mpdc == nullptr) return false;

	mpdc->AccessBoundingBoxes().SetObjectSpaceBBox(0.0f, 0.0f, 0.0f, 10.0f, 10.0f, 10.0f);
	mpdc->AccessBoundingBoxes().SetObjectSpaceClipBox(mpdc->AccessBoundingBoxes().ObjectSpaceBBox());
	mpdc->SetFrameCount(1);
	mpdc->SetDataHash(0);

	return true;
}
