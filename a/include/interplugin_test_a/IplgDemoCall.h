#pragma once
#include "interplugin_test_a/interplugin_test_a.h"
#include "mmcore/Call.h"
#include "mmcore/factories/CallAutoDescription.h"

namespace megamol {
namespace interplugin_test_a {

	class INTERPLUGIN_TEST_A_API IplgDemoCall : public core::Call {
	public:
		static const char *ClassName(void) {
			return "IplgDemoCall";
		}
		static const char *Description(void) {
			return "Demo-Call for a value";
		}
		static unsigned int FunctionCount(void) {
			return 1;
		}
		static const char * FunctionName(unsigned int idx) {
			switch (idx) {
			case 0: return "GetValue";
			default: return NULL;
			}
		}

		IplgDemoCall();
		virtual ~IplgDemoCall();

		inline int GetValue(void) const {
			return val;
		}
		inline void SetValue(int v) {
			val = v;
		}

	private:

		int val;

	};

	typedef core::factories::CallAutoDescription<IplgDemoCall> IplgDemoCallDescription;

}
}
